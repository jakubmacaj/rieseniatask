<?php
use Migrations\AbstractMigration;

class CreateImages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
	    $table = $this->table('images');

	    $table
		    ->addColumn('name', 'string', [
			    'null' => false,
			    'length' => 255,
		    ])
		    ->addColumn('top', 'integer', [
			    'null' => false,
			    'length' => 8
		    ])
		    ->addColumn('left', 'integer', [
			    'null' => false,
			    'length' => 8
		    ])
		    ->addColumn('width', 'integer', [
			    'null' => false,
			    'length' => 8
		    ])
		    ->addColumn('height', 'integer', [
			    'null' => false,
			    'length' => 8
		    ]);
	    $table->addPrimaryKey('id');
	    $table->create();
    }
}
