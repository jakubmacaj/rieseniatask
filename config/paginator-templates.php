<?php
return [
	'numbers' => '<span style="float:left;"><a href="{{url}}">{{text}}</a></span>',
	'current' => '<li class="active"><a href="#">{{text}}</a></li>',
	'nextActive' => '<li><a aria-label="Next" href="{{url}}">{{text}}</a></li>',
	'nextDisabled' => '<li class="next disabled"><a aria-label="Next"><span aria-hidden="true">»</span></a></li>',
	'prevActive' => '<li><a aria-label="Previous" href="{{url}}">{{text}}</a></li>',
	'prevDisabled' => '<li class="prev disabled"><a aria-label="Previous"><span aria-hidden="true">«</span></a></li>'
];