<?php
/**
 * Created by PhpStorm.
 * User: jacob
 * Date: 21.5.18
 * Time: 18:47
 */

namespace App\Model\Table;


use Cake\ORM\Table;
use Cake\Validation\Validator;

class ImagesTable extends Table
{
    public function initialize(array $config)
    {
        $this->setTable('images');
        $this->setPrimaryKey('id');
        $this->setEntityClass('App\Model\Entity\Image');
    }

    public function validationDefault(Validator $validator)
    {
        $validator->add('image', [
            'validExtension' => [
                'rule' => ['extension', ['jpg', 'png', 'gif', 'jpeg']],
                'message' => __('Invalid file extension')
            ]
        ]);
        return $validator;
    }
}