<?php
/**
 * Created by PhpStorm.
 * User: jacob
 * Date: 22.5.18
 * Time: 19:14
 */

namespace App\Model;


class SearchModel
{
    private $searchFields = [
        'sirka' => 'width',
        'vyska' => 'height',
        'nazov_suboru' => 'name'
    ];

    private $operators = [
        'CONTAINS' => 'LIKE',
        'EQUALS' => '=',
        '=' => '=',
        '>' => '>',
        '<' => '<'
    ];

    private $logicalOperators = [
        'AND',
        'OR'
    ];

    public function parseQueryObject($data): array
    {
        $queryObject = [];

        if ($this->validateBraces($data)) {
            $parts = $identifiedParts = [];

            $data = str_replace(array_keys($this->operators), $this->operators, $data);
            $data = str_replace(array_keys($this->searchFields), $this->searchFields, $data);
            preg_match_all('#(\".*?\"|\(.*?\))|[^ ]+#', $data, $parts);

            $identifiedParts = $this->iterateQueryParts($parts);

            $queryObject = $this->buildQueryObject($identifiedParts);
        }

        return $queryObject;
    }

    private function buildQueryObject($identifiedParts): array
    {
        $queryObject = [];
        foreach ($identifiedParts as $key => $part) {
            if (count(array_keys(array_column($identifiedParts, 'value'), 'OR')) == 1 && $orKey = array_search('OR', array_column($identifiedParts, 'value'))) {
                $firstKey = $identifiedParts[$orKey - 3]['value'] . ' ' . $identifiedParts[$orKey - 2]['value'];
                $secondKey = $identifiedParts[$orKey + 1]['value'] . ' ' . $identifiedParts[$orKey + 2]['value'];
                $queryObject['OR'][$firstKey] = $identifiedParts[$orKey - 1]['value'];
                $queryObject['OR'][$secondKey] = $identifiedParts[$orKey + 3]['value'];
                break;
            } else {
                switch ($part['type']) {
                    case 'field':
                        $newKey = $part['value'] . ' ' . ($identifiedParts[$key + 1]['type'] == 'operator' ? $identifiedParts[$key + 1]['value'] : null);
                        $queryObject[$newKey] = ($identifiedParts[$key + 1]['value'] == 'LIKE' ? str_replace('"', '%', $identifiedParts[$key + 2]['value']) : $identifiedParts[$key + 2]['value']);
                        break;
                    case 'logicalOperator':
                        continue;
                    case 'subQuery':
                        $newKey = $part['value'][array_search('logicalOperator', array_column($part['value'], 'type'))]['value'];
                        $queryObject[$newKey] = $this->buildQueryObject($part['value']);
                        break;
                }
            }
        }
        return $queryObject;
    }

    private function identifySubQueryParts($data): array
    {
        $parts = [];
        if (substr_count($data, '(') > 1) {
            preg_match_all('#(\".*?\"|\(.*?\))|[^ ]+#', $data, $parts);
        } else {
            $data = str_replace(['(', ')'], '', $data);
            preg_match_all('#(\".*?\")|[^ ]+#', $data, $parts);
            return $this->iterateQueryParts($parts);
        }
    }

    private function validateBraces($data): bool
    {
        return substr_count($data, '(') === substr_count($data, ')');
    }

    private function iterateQueryParts($parts): array
    {
        $identifiedParts = [];
        foreach ($parts[0] as $key => $part) {
            $type = $this->identifyPart($part);
            if ($type) {
                if ($type == 'subQuery') {
                    $identifiedParts[] = [
                        'value' => $this->identifySubQueryParts($part),
                        'type' => $type
                    ];
                } else {
                    $identifiedParts[] = [
                        'value' => $part,
                        'type' => $type
                    ];
                }
            } else {
                throw new \Exception("Unable to identify query part " . $part);
            }
        }
        return $identifiedParts;
    }

    private function identifyPart($part): string
    {
        $isSubQuery = (substr_count($part, '(') > 0 && substr_count($part, ')'));
        if ($isSubQuery) {
            return 'subQuery';
        }

        $isOperator = (in_array($part, $this->operators));
        if ($isOperator) {
            return 'operator';
        }

        $isLogicalOperator = (in_array($part, $this->logicalOperators));
        if ($isLogicalOperator) {
            return 'logicalOperator';
        }

        $isField = (in_array($part, $this->searchFields));
        if ($isField) {
            return 'field';
        }

        $isValue = (substr_count($part, '"') == 2 || is_int(intval($part)));
        if ($isValue) {
            return 'value';
        }

        return false;
    }

}