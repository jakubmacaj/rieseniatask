<?php
/**
 * Created by PhpStorm.
 * User: jacob
 * Date: 21.5.18
 * Time: 19:08
 */

namespace App\Model\Entity;


use Cake\ORM\Entity;

class Image extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}