<?php echo $this->Form->create('searchForm'); ?>
<?php echo $this->Form->control('search', ['type' => 'text']); ?>
<?php echo $this->Form->end(); ?>

<?php foreach($images as $image): ?>
<span><img src="<?php echo $uploadPath . $image['name']?>"></span>
<?php endforeach; ?>
<?php echo $this->Paginator->numbers(['class' => 'numbers']); ?>

<?php echo $this->Form->create('imageForm', ['enctype' => 'multipart/form-data']); ?>
<label for="image">Obrazok</label>
<?php echo $this->Form->file('image'); ?>
<?php echo $this->Form->control('left'); ?>
<?php echo $this->Form->control('top'); ?>
<?php echo $this->Form->control('width'); ?>
<?php echo $this->Form->control('height'); ?>
<?php echo $this->Form->button('submit'); ?>
<?php echo $this->Form->end(); ?>