<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/16/18
 * Time: 9:23 PM
 */

namespace App\Controller;


use App\Form\ImagesForm;
use App\Model\SearchModel;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\TableRegistry;

class ImagesController extends AppController
{
    public $paginate = [
        'limit' => 10,
        'order' => [
            'Image.id' => 'asc'
        ]
    ];

    public function display()
    {
        $this->loadComponent('Paginator');
        $locator = new TableLocator();
        $imagesTable = $locator->get('images');
        $uploadPath = 'uploads/files/';

        if ($this->getRequest()->is('post')) {
            $formData = $this->getRequest()->getData();

            if (isset($formData['image'])) {
                $fileName = $formData['image']['name'];
                $formData['name'] = $fileName;
                $image = $imagesTable->newEntity($formData);

                $imagesTable->save($image);
                if (!$errors = $image->getErrors()) {
                    $uploadFile = $uploadPath . $fileName;

                    if (move_uploaded_file($formData['image']['tmp_name'], $uploadFile)) {
                        $cropResult = $this->processUploadedImage($formData, $uploadFile);

                        if ($cropResult) {
                            $this->Flash->success(__('File has been uploaded and inserted successfully.'));
                        } else {
                            $this->Flash->error('Unable to crop image, please check for overlaping dimensions');
                        }

                    } else {
                        $this->Flash->error(__('Unable to upload file, please try again.'));
                    }
                } else {
                    $this->Flash->error(implode('\n', $errors));
                }
            } else {
                $searchModel = new SearchModel();
                $query = $searchModel->parseQueryObject($formData['search']);
                dump($query);
            }
        }

        $data = [
            'uploadPath' => DS . $uploadPath
        ];

        if (isset($query)) {
            $images = $this->Paginator->paginate($locator->get('images')->find()->where($query), $this->paginate);
        } else {
            $images = $this->Paginator->paginate($locator->get('images')->find(), $this->paginate);
        }
        if ($images) {
            $data['images'] = $images;
            $this->set(compact('image'));
        }

        $this->set($data);
        $this->render('/Images/index');
    }

    private function processUploadedImage($_data, $uploadedFile): bool
    {
        try {
            $imagick = new \Imagick(WWW_ROOT . $uploadedFile);
            $handle = fopen($uploadedFile, 'r+');
            $imagick->readImageFile($handle);
            if ($imagick->getImageWidth() >= $_data['width'] && $imagick->getImageHeight() >= $_data['height']) {
                $imagick->cropImage($_data['width'], $_data['height'], $_data['top'], $_data['height']);
                $imagick->writeImage($uploadedFile);
            } else {
                return false;
            }
        } catch (\ImagickException $e) {
            $this->Flash->error($e->getMessage());
        }
        return true;
    }

}